# symfony/[orm-pack](https://phppackages.org/p/symfony/orm-pack)

Doctrine ORM http://www.doctrine-project.org

## Official documentation
* [*Databases and the Doctrine ORM*
  ](https://symfony.com/doc/current/doctrine.html)

## Unofficial documentation
* [*10 Commands to Add a Database to Symfony:*
  ](https://faun.pub/10-commands-to-add-a-database-to-symfony-b4e3cdfa5fd2)
  The Beginner-friendly tutorial about Doctrine
  2023-01 Nico Anastasio
* [*How to make Doctrine (way) faster*
  ](https://medium.com/@edouard.courty/how-to-make-doctrine-way-faster-63e6a2f1c77f)
  2022-07 Edouard Courty
* [*Doctrine and Symfony, setup and usage tutorial*
  ](https://medium.com/@web_hints/doctrine-and-symfony-setup-and-usage-tutorial-799f9e56cba1)
  2018 Ciryk Popeye
